#! /usr/bin/env python3

# Copyright 2019 Richard Kjerstadius
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import platform
import shutil
import subprocess
import sys


class colors:
    if platform.system() == 'Windows':
        END = ''
        FAIL = ''
        PASS = ''
    else:
        END = '\033[0m'
        FAIL = '\033[91m'
        PASS = '\033[92m'


def get_ancestor_sha(base_branch='origin/master', commit='HEAD'):
    try:
        process = subprocess.run(['git', 'merge-base', '--fork-point',
                                  base_branch, commit], check=True,
                                 capture_output=True, encoding='utf8')
    except subprocess.CalledProcessError as cpe_error:
        print(cpe_error)
        print('Problem encountered when trying to identify the common ancestor'
              ' commit. Is the base branch really a valid ref?')
        raise
    else:
        return process.stdout.strip()


def git_clang_format(baseline_commit):
    process = subprocess.run(['git', 'clang-format', baseline_commit],
                             capture_output=True, encoding='utf8')
    if 'changed files:' in process.stdout:
        files = [line.strip()
                 for line in process.stdout.split('\n')[1:-1]]
        return files
    else:
        return None


def is_tree_dirty(commit='HEAD'):
    # Can use diff-index also. Don't really know if it makes any meaningful
    # difference.
    process = subprocess.run(['git', 'diff', '--quiet', commit])
    if process.returncode != 0:
        return True
    else:
        return False


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Simple script for ensuring '
                                     'that clang-format style rules are '
                                     'upheld.')
    parser.add_argument('-b', '--base-branch', default='origin/master',
                        help='Base branch for comparison. I.e. the baseline '
                        'for the comparison, meaning only the diff between it '
                        'and HEAD are considered. For a merge request this is'
                        'normally origin/master (which is also the default).')
    cmd_args = parser.parse_args()

    if shutil.which('git') is None:
        print('Git executable not found. Exiting.')
        sys.exit(1)
    if is_tree_dirty():
        print('Work tree must be clean when running the script. Please commit '
              'or stash your changes.')
        sys.exit(1)

    try:
        ancestor_sha = get_ancestor_sha(cmd_args.base_branch)
        modified_files = git_clang_format(ancestor_sha)
    except subprocess.CalledProcessError:
        sys.exit(1)
    else:
        if modified_files is not None and is_tree_dirty():
            print('Style deviations detected in the following file(s):')
            for file in modified_files:
                print(' - ' + file)
            print('Please run clang-format on all new and modified code.')
            print(colors.FAIL + 'Style check failed.' + colors.END)
            sys.exit(1)
        else:
            print(colors.PASS + 'Style check passed.' + colors.END)
            sys.exit(0)
