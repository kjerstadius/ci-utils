# Copyright 2019 Richard Kjerstadius
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from . import badges


def main():
    import argparse
    import sys
    parser = argparse.ArgumentParser(description='''Script for generating
                                     shields.io json endpoints for custom
                                     badges.''')
    parser.add_argument('--complexity', help='''Lizard code complexity json
                        report filename. When this option is given, the
                        generator will create all badge endpoints associated
                        with Lizard code complexity metrics.''', default=None)
    parser.add_argument('--codeclimate', default=None)
    parser.add_argument('-o', '--output-path', default=None)
    cmd_args = parser.parse_args()

    all_badges = []
    if cmd_args.complexity is not None:
        all_badges.append(badges.ComplexityBadges(cmd_args.complexity,
                                                  cmd_args.output_path))

    if cmd_args.codeclimate is not None:
        all_badges.append(badges.CodeClimateBadges(cmd_args.codeclimate,
                                                   cmd_args.output_path))

    for badge_obj in all_badges:
        badge_obj.dump()

    sys.exit(0)
