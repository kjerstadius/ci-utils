# Copyright 2019 Richard Kjerstadius
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from collections import Counter
import json
import os
import re


class ShieldsioBadge():
    def __init__(self, message, label='', color='grey', named_logo=None,
                 output_filename=None):
        self.color = color
        self.label = label
        self.message = message
        self.named_logo = named_logo
        self.output_filename = output_filename
        self.schema_version = 1

    def __str__(self):
        return str(self.get_badge_dict())

    def get_badge_dict(self):
        badge_dict = {
            'schemaVersion': self.schema_version,
            'label': self.label,
            'message': str(self.message),
        }
        if isinstance(self.color, (dict, tuple)):
            badge_dict['color'] = self.get_color_from_range()
        else:
            badge_dict['color'] = self.color
        if self.named_logo is not None:
            badge_dict['namedLogo'] = self.named_logo

        return badge_dict

    def get_color_from_range(self):
        '''Return the status color associated with the badge value.'''
        value = self.message
        for current_range in self.color:
            if current_range.get('end') not in [None, '']:
                if int(value) in range(current_range['start'],
                                       current_range['end']):
                    return current_range['color']
            else:
                if int(value) >= current_range['start']:
                    return current_range['color']
        return 'grey'

    def dump(self):
        badge_dict = self.get_badge_dict()
        if self.output_filename is not None:
            with open(self.output_filename, 'w') as outfile:
                json.dump(badge_dict, outfile, indent=4)
        else:
            print(json.dumps(badge_dict, indent=4))


class CountFilter():
    def __init__(self, key, value_filter=None):
        self.key = key
        self.value_filter = value_filter
        if value_filter is not None and not isinstance(value_filter,
                                                       (list, tuple)):
            self.value_filter = [self.value_filter]

    def _get_all_values(self, data):
        '''Return a list with all values of key in data.'''
        collection_list = []
        for item in data:
            if isinstance(item, dict):
                collection_list.extend(self._get_all_values(item))
            elif isinstance(item, (list, tuple)):
                for list_item in item:
                    collection_list.extend(self._get_all_values(list_item))
            else:
                if item == self.key:
                    collection_list.append(data[item])
        return collection_list

    def _count(self, report):
        '''Return the number of occurrences of key and optionally when the
        value of key is in or equal to value_filter.'''
        all_values = self._get_all_values(report)
        counter = Counter(all_values)
        filtered_sum = 0
        value_filter = self.value_filter if self.value_filter is not None \
            else counter.keys()
        for value in value_filter:
            filtered_sum += counter[value]
        return filtered_sum

    def filter(self, report):
        return self._count(report)


def badge_from_report(wrap_class):
    def _get_logo():
        '''Check if wrap_class defines a named logo that should be used.'''
        try:
            logo = wrap_class.logo
        except AttributeError:
            logo = None
        return logo

    def _get_output_filename(path):
        '''Return the absolute path to the output file, or None.'''
        if path is not None:
            basename = '.'.join([_sanitize_name(wrap_class.group),
                                 _sanitize_name(wrap_class.label), 'json'])
            pathname = os.path.abspath(path)
            output_filename = os.path.join(pathname, basename)
        else:
            output_filename = None
        return output_filename

    def _get_value_from_report(report):
        '''Return the filtered or unfiltered value from report.'''
        with open(report, 'r') as infile:
            report_data = json.load(infile)

        # Try to filter the report data, otherwise just get the value
        try:
            value = wrap_class.report_filter.filter(report_data)
        except AttributeError:
            value = report_data[wrap_class.report_key]
        return value

    def _sanitize_name(name):
        '''Sanitize name to ensure filename suitability.'''
        name = str(name)
        name = re.sub(r'[^\w\s-]', '', name).strip().lower()
        return re.sub(r'[-\s]+', '-', name)

    class ReportBadgeClass(ShieldsioBadge):
        def __init__(self, report, output_path=None):
            super().__init__(_get_value_from_report(report),
                             label=wrap_class.label,
                             color=wrap_class.color,
                             named_logo=_get_logo(),
                             output_filename=_get_output_filename(output_path))

    return ReportBadgeClass


class BadgeGroup():
    def dump(self):
        for badge in self.badges:
            badge.dump()


if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser(description='''Script for generating
        shields.io json endpoints for custom badges.''')
    parser.add_argument('message', help='''The text on the right side of the
                        badge.''')
    parser.add_argument('-l', '--label', default='', help='''The text on the
                        left side of the badge. Can be left empty.''')
    parser.add_argument('-c', '--color', default='grey', help='''The right side
                        color. Default is grey.''')
    parser.add_argument('-o', '--output', default=None, help='''Write output to
                        a file with the name given. If not provided, output
                        will be written to stdout.''')
    cmd_args = parser.parse_args()

    badge = ShieldsioBadge(cmd_args.message, label=cmd_args.label,
                           color=cmd_args.color)
    badge.dump(cmd_args.output)

    sys.exit(0)
