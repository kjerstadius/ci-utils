# Copyright 2019 Richard Kjerstadius
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from .shieldsio_badge import badge_from_report, BadgeGroup, CountFilter


class ComplexityBadges(BadgeGroup):
    group = 'Complexity'

    def __init__(self, report, output_path=None):
        types_in_group = [ComplexityCCN, ComplexityKloc,
                          ComplexityWarnings]
        self.badges = [badge(report, output_path) for badge in types_in_group]


@badge_from_report
class ComplexityCCN(ComplexityBadges):
    # These limits are inspired by "the magical number 7, plus or minus 2"
    color = (
        {
            'start': 0,
            'end': 8,
            'color': 'brightgreen'
        },
        {
            'start': 8,
            'end': 13,
            'color': 'yellow'
        },
        {
            'start': 13,
            'end': 15,
            'color': 'orange'
        },
        {
            'start': 15,
            'color': 'red'
        }
    )
    label = 'Average complexity (CCN)'
    report_key = 'average_complexity'


@badge_from_report
class ComplexityKloc(ComplexityBadges):
    color = 'blue'
    label = 'kLOC'
    report_key = 'kloc'


@badge_from_report
class ComplexityWarnings(ComplexityBadges):
    color = (
        {
            'start': 0,
            'end': 1,
            'color': 'brightgreen'
        },
        {
            'start': 1,
            'end': 10,
            'color': 'yellow'
        },
        {
            'start': 10,
            'end': 20,
            'color': 'orange'
        },
        {
            'start': 20,
            'color': 'red'
        }
    )
    label = 'Complexity warnings'
    report_key = 'warning_count'


class CodeClimateBadges(BadgeGroup):
    group = 'Code Climate'

    def __init__(self, report, output_path=None):
        types_in_group = [CodeClimateBlockerCritical, CodeClimateMajor,
                          CodeClimateMinor]
        self.badges = [badge(report, output_path) for badge in types_in_group]


@badge_from_report
class CodeClimateBlockerCritical(CodeClimateBadges):
    color = (
        {
            'start': 0,
            'end': 1,
            'color': 'brightgreen'
        },
        {
            'start': 1,
            'end': 5,
            'color': 'orange'
        },
        {
            'start': 5,
            'color': 'red'
        }
    )
    label = 'Blocker & Critical'
    logo = 'Code Climate'
    report_filter = CountFilter('severity', ('blocker', 'critical'))


@badge_from_report
class CodeClimateMajor(CodeClimateBadges):
    color = (
        {
            'start': 0,
            'end': 1,
            'color': 'brightgreen'
        },
        {
            'start': 1,
            'end': 10,
            'color': 'yellow'
        },
        {
            'start': 10,
            'end': 20,
            'color': 'orange'
        },
        {
            'start': 20,
            'color': 'red'
        }
    )
    label = 'Major'
    logo = 'Code Climate'
    report_filter = CountFilter('severity', 'major')


@badge_from_report
class CodeClimateMinor(CodeClimateBadges):
    color = (
        {
            'start': 0,
            'end': 5,
            'color': 'brightgreen'
        },
        {
            'start': 5,
            'end': 20,
            'color': 'yellow'
        },
        {
            'start': 20,
            'end': 50,
            'color': 'orange'
        },
        {
            'start': 50,
            'color': 'red'
        }
    )
    label = 'Minor'
    logo = 'Code Climate'
    report_filter = CountFilter('severity', 'minor')
